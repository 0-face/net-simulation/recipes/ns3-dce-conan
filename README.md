# ns3 DCE (Direct Code Execution) - conan package

[Conan.io](https://www.conan.io/) package for [ns3-DCE][ns3_dce_home].

> Direct Code Execution (DCE) is a framework for ns-3 that provides facilities
  to execute, within ns-3, existing implementations of userspace and kernelspace
  network protocols or applications without source code changes. For example,
  instead of using ns-3's implementation of a ping-like application, you can use
  the real ping application. You can also use the Linux networking stack in simulations.


[ns3_dce_home]: https://www.nsnam.org/overview/projects/direct-code-execution/


## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
[DCE documentation][dce_docs] to instructions in how to use DCE.

Note that to execute a compiled DCE script, you need to setup (at least) two environment
variables:

* **DCE_PATH** - Should contain one or more paths (separated by ':') where the
  executables runned by DCE are located.
* **LD_LIBRARY_PATH** - Should point to the path where DCE libs are compiled. This
  recipe already sets this variable (through conan 'env_info' variables).
  See [conan docs about env_info][env_info_docs] for instructions about it.

See also the 'test_package' recipe for an example in how to execute a script.

[dce_docs]: https://ns-3-dce.readthedocs.io/en/latest/index.html
[env_info_docs]: https://docs.conan.io/en/latest/reference/conanfile/methods.html#env-info


## License

This conan package is distributed under the [unlicense](http://unlicense.org/)
terms (see UNLICENSE.md).

DCE, however, is distributed under the [GNU GPLv2 license](http://www.gnu.org/copyleft/gpl.html).


## Limitations

This package currently does not expose all available options for DCE.
Specially, it does not build yet the python wrappers.

Please, contribute to improve the package.
