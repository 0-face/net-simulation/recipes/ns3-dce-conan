# To speed up 'apt-get update', we will update only
# from some repositories

# We get distribution version
cat /etc/lsb-release
. /etc/lsb-release

# Then, create a file with the repositories to update from
echo "deb http://archive.ubuntu.com/ubuntu/ $DISTRIB_CODENAME main" > source.list
echo "deb http://archive.ubuntu.com/ubuntu/ $DISTRIB_CODENAME-updates main" >> source.list
echo "deb http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu/ $DISTRIB_CODENAME main" >> source.list

# Update only that repository
sudo apt-get update -o Dir::Etc::sourcelist="./source.list" \
    -o Dir::Etc::sourceparts="-" -o APT::Get::List-Cleanup="0"

# Install dependencies
sudo apt-get install -y libc6-dev:i386 bison flex
