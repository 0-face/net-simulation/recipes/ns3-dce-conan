#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools, AutoToolsBuildEnvironment
from conans.errors import ConanInvalidConfiguration

from os import path

class Recipe(ConanFile):
    name        = "ns3-dce"
    version     = "1.10"
    license     = "GPL 2.0"
    description = "Direct Code Execution (DCE) is a framework for ns-3 that allows " \
                  + "to execute, within ns-3, existing implementations of network " \
                  + "protocols or applications without source code changes."
    settings    = "os", "compiler", "build_type", "arch"

    homepage    = "https://www.nsnam.org/overview/projects/direct-code-execution/"
    url         = "https://gitlab.com/0-face/conan-recipes/ns3-dce-conan"

    requires = (
        "ns3/3.28@noface/stable"
    )

    options = {
        'enable_linux_stack': [True, False],
        'build_examples': [True, False]
    }
    default_options = (
        'enable_linux_stack=True',
        'build_examples=False'
    )

    keep_imports    = True #used to repackage 'ip' executable

    RELEASE_DIR     = "ns-3-dce-dce-" + version
    RELEASE_FILE    = RELEASE_DIR + ".tar.gz"
    RELEASE = {
        'source': "https://github.com/direct-code-execution/ns-3-dce/archive/dce-1.10.tar.gz",
        'sha256': "9dd83890e739bdb9735a93ccb8c7193ac7463c035ded6e8ad95be196269023af"
    }

    def build_requirements(self):
        if self.linux_stack_enabled:
            self.build_requires("iproute2/4.4.0@noface/stable")

    def requirements(self):
        if self.linux_stack_enabled:
            self.requires("net-next-nuse/4.4@noface/stable")

    def configure(self):
        if self.settings.compiler != "gcc":
            raise ConanInvalidConfiguration("DCE only supports GCC (for now)")

        if self.linux_stack_enabled:
            self.options["iproute2"].extra_cflags = "-fPIC -U_FORTIFY_SOURCE -g"
            self.options["iproute2"].extra_ldflags = "-pie -rdynamic"

    def imports(self):
        self.copy("ip", root_package="iproute2", src="sbin", dst="bin")

    def source(self):
        tools.get(self.RELEASE['source'], sha256 = self.RELEASE['sha256'])

    def build(self):
        self.patch_sources(strict=True)

        env_build = AutoToolsBuildEnvironment(self)

        if self.settings.arch == "x86" and self.settings.compiler == "gcc":
            #fixing error in build: undefined reference to `__stack_chk_fail_local'
            env_build.flags.append('-fno-stack-protector')

        env_vars = env_build.vars

        # Remove '-DNDEBUG' option to avoid error in build (missing '__assert_fail' function)
        env_vars['CPPFLAGS'] = env_vars['CPPFLAGS'].replace('-DNDEBUG', '')

        with tools.environment_append(env_vars):
            self.run_and_log(
                    cmd = "./waf configure build install " + self.waf_options(),
                    cwd = self.dce_source_folder)

    def package(self):
        # Copies all 'install_dir' to package
        self.copy("*", src=self.install_dir, symlinks=True)

        self.copy("ip", src="bin", dst="bin")

    def package_info(self):
        self.cpp_info.libs    = ['ns3-dce', 'ns3-netlink']
        self.cpp_info.libdirs = ['lib']

        self.setup_ld_library_path()

#################################### Steps ###########################################

    def patch_sources(self, strict=False):

        # Fix search of pkgconfig files
        tools.replace_in_file(
            path.join(self.dce_source_folder, 'ns3waf', '__init__.py'),
            """
            pcfiles = glob.glob(conf.env['NS3_DIR'] + '/lib*/pkgconfig/' + 'libns%s*-%s-%s*'
                                % (ver, module.lower(), conf.env['LIB_SUFFIX']))""",
            """
            pcfiles = glob.glob(conf.env['NS3_DIR'] + '/lib*/pkgconfig/' + 'libns3*-%s*'
                                % (module.lower()))"""
            , strict=strict)


        # Fix compilation errors when disabling tests and examples
        dce_wscript_file = path.join(self.dce_source_folder, 'wscript')

        tools.replace_in_file(
            dce_wscript_file,
'''
    build_dce_tests(module, bld)
    build_dce_examples(module, bld)
''',
'''
    if bld.options.enable_tests:
        build_dce_tests(module, bld)
    if bld.options.enable_examples:
        build_dce_examples(module, bld)
'''
            , strict=strict
        )

        # clang is not supported (yet) by ns3-dce
#        if self.settings.compiler == "clang":
#            tools.replace_in_file(
#                dce_wscript_file,
#                "'-fno-test-coverage',",
#                "",
#                strict=strict
#            )

        dce_model_dir = path.join(self.dce_source_folder, 'model')

        tools.replace_in_file(
            path.join(dce_model_dir, 'dce-syslog.cc'),
            "  vfprintf (process->syslog, message, args);",
            '\n'.join([
                "",
                "  if(process->syslog == NULL){ //open syslog if needed",
                "    dce_openlog(NULL, 0, 0);",
                "  }",
                "",
                "  vfprintf (process->syslog, message, args);"
            ])
            , strict=strict
        )

        libc_ns3_file = path.join(dce_model_dir, 'libc-ns3.h')

        # Fixing error in build for x86
        tools.replace_in_file(
            libc_ns3_file,
            'DCE_EXPLICIT (__poll_chk, int, struct pollfd *, long unsigned int, int, long unsigned int)',
            'DCE_EXPLICIT (__poll_chk, int, struct pollfd *, long unsigned int, int, size_t)',
            strict=strict
        )

        #localtime_r should be declared as NATIVE
        self.insert_at_file(
            libc_ns3_file,
            "NATIVE (ctime_r)",
            "NATIVE (localtime_r)"
            , strict=strict
        )

        self.insert_at_file(
            path.join(dce_model_dir, 'libc-global-variables.cc'),
            "weak_alias (dce_proginvnameshort, program_invocation_short_name);",
            """
char *__tzname[2] = { (char *) "GMT", (char *) "GMT" };
int __daylight = 0;
long int __timezone = 0L;

weak_alias (__tzname, tzname);
weak_alias (__daylight, daylight);
weak_alias (__timezone, timezone);""",
            strict=strict
        )

        # Add missing pthread functions

        self.insert_at_file(
            libc_ns3_file,
            "DCE (pthread_condattr_init)",
            '\n'.join([
                "DCE (pthread_condattr_getclock)",
                "DCE (pthread_condattr_setclock)",
                "DCE (pthread_attr_init)",
                "DCE (pthread_attr_destroy)",
            ]),
            strict=strict
        )

        self.insert_at_file(
            path.join(dce_model_dir, 'dce-pthread.h'),
            "int dce_pthread_condattr_init (pthread_condattr_t *attr);",
            '\n'.join([
                "int dce_pthread_condattr_getclock (const pthread_condattr_t * attr, clockid_t * clock_id);",
                "int dce_pthread_condattr_setclock (pthread_condattr_t *__attr, clockid_t clock_id);",
                "int dce_pthread_attr_init(pthread_attr_t *attr);",
                "int dce_pthread_attr_destroy(pthread_attr_t *attr);"
            ]),
            strict=strict
        )

        self.insert_at_file(
            path.join(dce_model_dir, 'dce-pthread-cond.cc'),
'''int dce_pthread_condattr_destroy (pthread_condattr_t *attr)
{
  return 0;
}''',
'''
int dce_pthread_condattr_getclock (const pthread_condattr_t * attr, clockid_t * clock_id){
    return 0; //indicates succesful result
}
int dce_pthread_condattr_setclock (pthread_condattr_t *__attr, clockid_t clock_id){
    return 0; //indicates succesful result
}
int dce_pthread_attr_init(pthread_attr_t *attr){
    return 0; //success
}
int dce_pthread_attr_destroy(pthread_attr_t *attr){
    return 0; //success
}
''',
            strict=strict
        )

    def setup_ld_library_path(self):
        ''' DCE scripts needs to load DCE libs at runtime,
            so we append the DCE lib path to the 'LD_LIBRARY_PATH'
            env var (or similar, according to the os).
        '''

        ld_lib_var = {
            'Macos': 'DYLD_LIBRARY_PATH',
            'Windows': 'PATH'
        }.get(self.settings.get_safe('os'), 'LD_LIBRARY_PATH')

        lib_path = path.join(self.package_folder, 'lib')

        getattr(self.env_info, ld_lib_var).append(lib_path)
        self.env_info.DCE_LIBRARY_PATH = lib_path

#################################### Properties ###########################################

    @property
    def dce_source_folder(self):
        return path.join(self.source_folder, self.RELEASE_DIR)

    @property
    def out_folder(self):
        return path.join(self.build_folder, 'build')

    @property
    def install_dir(self):
        return path.abspath(path.join(self.build_folder, "install"))

    @property
    def ns3_version_name(self):
        if self.version == 'dev':
            return 'ns-dev'
        else:
            return 'ns' + self.version

    @property
    def linux_stack_enabled(self):
        return self.options.enable_linux_stack in [True, 'True']

#################################### Helpers ###########################################

    def run_and_log(self, cmd, cwd=None):
        msg = ''
        if cwd:
            msg = "[{}] ".format(cwd)

        self.output.info(msg + cmd)

        self.run(cmd, cwd = cwd)

    def waf_options(self):
        opts = []

#        opts.append("-v")
        opts.append("--out='{}'"   .format(self.out_folder))
        opts.append("--prefix='{}'".format(self.install_dir))

        opts.append("--with-ns3=" + self.deps_cpp_info["ns3"].rootpath)
        opts.append("--disable-tests")

        if self.linux_stack_enabled:
            net_next_sim_root = self.deps_cpp_info["net-next-nuse"].rootpath
            opts.append("--enable-kernel-stack={}".format(net_next_sim_root))

        if self.options.build_examples == False:
            opts.append("--disable-examples")

        if self.settings.build_type != "Debug":
            opts.append("--disable-debug")

        return " ".join(opts)

    def insert_at_file(self, filepath, at, content, strict=True, add_linebreak=True):
        replace_with = at + ('\n' if add_linebreak else '') + content

        tools.replace_in_file(
            filepath,
            at,
            replace_with,
            strict=strict
        )
