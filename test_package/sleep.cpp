#include <unistd.h> //sleep

#include <iostream>
#include <string>
#include <cstdio>

int main(int argc, char *argv[]){
    int sleepSeconds = 5;

    if(argc > 1){
        sleepSeconds = std::stoi(argv[1]);
    }

// NOTE: looks like there is a bug related to DCE when trying to print numbers with streams
// (the rest of output is not saved). So, we use printf, instead.
//    std::cout << "Sleeping for " << sleepSeconds << " seconds" << std::endl;
    printf("Sleeping for %d seconds\n", sleepSeconds);

    sleep(sleepSeconds);

    std::cout << "Finished sleeping" << std::endl;

    return 0;
}
